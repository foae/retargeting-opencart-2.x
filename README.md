# Retargeting extension for OpenCart 2.0+ #

### Extension available on: ###
* OpenCart official extensions directory [[link](http://www.opencart.com/index.php?route=extension/extension/info&extension_id=22080&filter_search=retargeting)]  
* Github [[link](https://github.com/retargeting/OpenCart-2.x)]  
* [Screenshot right here](http://i.imgur.com/AkA2C5j.jpg)  
  
### What is Retargeting? ###
https://retargeting.biz/en/features